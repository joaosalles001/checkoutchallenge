﻿using System;
using System.Threading.Tasks;
using CheckoutChallenge.Logger.Interface;
using Nest;

namespace CheckoutChallenge.Logger.Implementation
{
    public class ElasticsearchLogRepository : ILogRepository
    {
        private ElasticClient _client;

        public ElasticsearchLogRepository(string connectionString)
        {
            var settings = new ConnectionSettings(new Uri(connectionString));
            _client = new ElasticClient(settings);
        }

        public bool Insert<T>(T request, string resourceName) where T : class
        {
            var indexResponse = _client.Index(request, i => i.Index(resourceName.ToLower()));
            return indexResponse.Result == Result.Created;
        }

        public async Task<bool> InsertAsync<T>(T request, string resourceName) where T : class
        {
            var indexResponse = await _client.IndexAsync(request, i => i.Index(resourceName.ToLower()));
            return indexResponse.Result == Result.Created;
        }
    }
}
