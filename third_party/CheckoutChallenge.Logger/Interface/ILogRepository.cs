﻿using System.Threading.Tasks;

namespace CheckoutChallenge.Logger.Interface
{
    public interface ILogRepository
    {
        bool Insert<T>(T request, string resourceName) where T : class;
        Task<bool> InsertAsync<T>(T request, string resourceName) where T : class;
    }
}
