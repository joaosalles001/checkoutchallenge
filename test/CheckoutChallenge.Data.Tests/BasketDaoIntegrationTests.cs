using NUnit.Framework;
using CheckoutChallenge.Data.Interface;
using CheckoutChallenge.Data.Implementation;
using CheckoutChallenge.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CheckoutChallenge.Bll.Tests
{
    [TestFixture]
    public class BasketDaoIntegrationTests
    {        
        private IBasketDao InMemoryBasketDao { get; set; }
        private IClientDao InMemoryClientDao { get; set; }

        // just so I know how many products I've seeded to the database
        private int MinProductId = 1;
        private int MaxProductId = 8;

        [SetUp]
        public void Setup()
        {
            var dbOptions = new DbContextOptionsBuilder<ChallengeContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            // use the same context so we have the pre-defined products in the database already
            var dbContext = new ChallengeContext(dbOptions.Options);

            InMemoryBasketDao = new BasketDao(dbContext);
            InMemoryClientDao = new ClientDao(dbContext);
        }

        [Test]
        public void GetByClient_NonExistentClient_ReturnsNull()
        {
            // this client id does not exist
            var clientBasket = InMemoryBasketDao.GetByClient(Int32.MaxValue);
            Assert.IsNull(clientBasket);
        }

        [Test]
        public void Basket_CreateInsertAndDeleteOperations()
        {
            var random = new Random((int)DateTime.Now.Ticks);

            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.Create(name);

            // add some products to the basket
            for (int i = MinProductId; i <= MaxProductId; i++)
            {
                InMemoryBasketDao.AddProduct(client.Id, i, random.Next(1, 100));
            }

            // get the list back from the database
            var clientBasket = InMemoryBasketDao.GetByClient(client.Id);
            Assert.AreEqual(MaxProductId, clientBasket.ClientProducts.Count());

            // now remove all the items
            foreach (var cp in clientBasket.ClientProducts.ToList())
            {
                InMemoryBasketDao.RemoveProduct(cp.ClientId, cp.ProductId);
            }

            // and make sure the new list is empty
            var newBasket = InMemoryBasketDao.GetByClient(client.Id);
            CollectionAssert.IsEmpty(newBasket.ClientProducts);
        }

        [Test]
        public void Basket_CreateInsertAndUpdateOperations()
        {
            var random = new Random((int)DateTime.Now.Ticks);

            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.Create(name);

            // add a product to the basket
            var oldQuantity = random.Next(1, 100);
            var productId = random.Next(MinProductId, MaxProductId);
            InMemoryBasketDao.AddProduct(client.Id, productId, oldQuantity);

            var oldBasket = InMemoryBasketDao.GetByClient(client.Id);
            var oldProduct = oldBasket.ClientProducts.First(cp => cp.ProductId == productId);
            Assert.AreEqual(oldQuantity, oldProduct.Quantity);

            // and change the quantity
            var newQuantity = random.Next(1, 100);
            InMemoryBasketDao.EditProductQuantity(client.Id, productId, newQuantity);

            var newBasket = InMemoryBasketDao.GetByClient(client.Id);
            var newProduct = oldBasket.ClientProducts.First(cp => cp.ProductId == productId);
            Assert.AreEqual(newQuantity, newProduct.Quantity);
        }

        [Test]
        public void Basket_TriesToClearAlreadyEmptyBasket_DoesNothing()
        {
            // generate random 15 character string
            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.Create(name);

            // make sure there's no product in the basket
            var clientBasket = InMemoryBasketDao.GetByClient(client.Id);
            CollectionAssert.IsEmpty(clientBasket.ClientProducts);

            // tries to empty it
            Assert.DoesNotThrow(() => InMemoryBasketDao.EmptyBasket(client.Id));
        }

        [Test]
        public void Basket_InsertItemsAndEmptyBasket()
        {
            var random = new Random((int)DateTime.Now.Ticks);

            // generate random 15 character string
            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.Create(name);

            for (int i = MinProductId; i <= MaxProductId; i++)
            {
                InMemoryBasketDao.AddProduct(client.Id, i, random.Next(1, 100));
            }

            // make sure the products are in the basket
            var clientBasket = InMemoryBasketDao.GetByClient(client.Id);
            CollectionAssert.IsNotEmpty(clientBasket.ClientProducts);

            // now empty it
            Assert.DoesNotThrow(() => InMemoryBasketDao.EmptyBasket(client.Id));

            // and verify it's empty
            var newClientBasket = InMemoryBasketDao.GetByClient(client.Id);
            CollectionAssert.IsEmpty(newClientBasket.ClientProducts);
        }
    }
}
