﻿using CheckoutChallenge.Data.Implementation;
using CheckoutChallenge.Data.Interface;
using CheckoutChallenge.Data.Model;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutChallenge.Data.Tests
{
    [TestFixture]
    public class ClientDaoIntegrationTests
    {
        private IClientDao InMemoryClientDao { get; set; }

        [SetUp]
        public void Setup()
        {
            var dbOptions = new DbContextOptionsBuilder<ChallengeContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            // use the same context so we have the pre-defined products in the database already
            var dbContext = new ChallengeContext(dbOptions.Options);

            InMemoryClientDao = new ClientDao(dbContext);
        }

        [Test]
        public void Create_ValidName_ReturnsClient()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.Create(name);
            Assert.IsNotNull(client);
            Assert.AreEqual(client.Name, name);
            Assert.IsTrue(client.Id > 0);
        }

        [Test]
        public void GetByName_InvalidName_ReturnsNull()
        {
            // this name does not exist
            var name = Guid.NewGuid().ToString().Substring(0, 15);
            var client = InMemoryClientDao.GetByName(name);
            Assert.IsNull(client);
        }

        [Test]
        public void GetByName_ValidName_ReturnsClient()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 15);
            InMemoryClientDao.Create(name);

            var client = InMemoryClientDao.GetByName(name);
            Assert.IsNotNull(client);
            Assert.AreEqual(name, client.Name);
        } 
    }
}
