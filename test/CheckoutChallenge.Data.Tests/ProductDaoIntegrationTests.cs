using NUnit.Framework;
using CheckoutChallenge.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using CheckoutChallenge.Data.Model;
using CheckoutChallenge.Data.Implementation;

namespace CheckoutChallenge.Bll.Tests
{
    [TestFixture]
    public class ProductDaoIntegrationTests
    {
        private IProductDao InMemoryProductDao { get; set; }

        // just so I know how many products I've seeded to the database
        private int MinProductId = 1;
        private int MaxProductId = 8;

        [SetUp]
        public void Setup()
        {
            var dbOptions = new DbContextOptionsBuilder<ChallengeContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            // use the same context so we have the pre-defined products in the database already
            var dbContext = new ChallengeContext(dbOptions.Options);

            InMemoryProductDao = new ProductDao(dbContext);
        }

        [Test]
        public void List_ReturnsEveryProduct()
        {
            var productList = InMemoryProductDao.List();
            Assert.IsNotNull(productList);
            CollectionAssert.IsNotEmpty(productList);
        }

        [Test]
        public void GetByCode_NonExistentCode_ReturnsNull()
        {
            var product = InMemoryProductDao.GetByCode(Guid.NewGuid().ToString());
            Assert.IsNull(product);
        }

        [Test]
        public void GetByCode_ValidCode_ReturnSingleProduct()
        {
            var productList = InMemoryProductDao.List();
            foreach (var product in productList)
            {
                var fetched = InMemoryProductDao.GetByCode(product.Code);
                Assert.IsNotNull(fetched);
                CollectionAssert.Contains(productList, fetched);
                Assert.AreEqual(product.Code, fetched.Code);
            }
        }

        [Test]
        public void GetById_InvalidId_ReturnsNull()
        {
            var product = InMemoryProductDao.GetById(Int32.MaxValue);
            Assert.IsNull(product);
        }

        [Test]
        public void GetById_ValidId_ReturnsSingleProduct()
        {
            for (int i = MinProductId; i <= MaxProductId; i++)
            {
                var product = InMemoryProductDao.GetById(i);
                Assert.IsNotNull(product);
                Assert.AreEqual(i, product.Id);
            }
        }
    }
}
