# THE CHALLENGE:


## Part 1:
Your company has decided to create a new line of business.  As a start to this effort, they’ve come to you to help develop a prototype.  It is expected that this prototype will be part of a beta test with some actual customers, and if successful, it is likely that the prototype will be expanded into a full product.

Your part of the prototype will be to develop a Web API that will be used by customers to manage a basket of items. The business describes the basic workflow is as follows:

This API will allow our users to set up and manage an order of items.  The API will allow users to add and remove items and change the quantity of the items they want.  They should also be able to simply clear out all items from their order and start again.

The functionality to complete the purchase of the items will be handled separately and will be written by a different team once this prototype is complete.  

For the purpose of this exercise, you can assume there’s an existing data storage solution that the API will use, so you can either create stubs for that functionality or simply hold data in memory.

Feel free to make any assumptions whenever you are not certain about the requirements, but make sure your assumptions are made clear either through the design or additional documentation.

## Part 2:
Create a client library that makes use of the API endpoints created in Part 1.  The purpose of this code to provide authors of client applications a simple framework to use in their applications.



# RUNNING INSTRUCTIONS:


To build a docker image and run it exposing the 5000 port (without ELK integration):

```
docker build -t checkoutchallenge-backend .
docker run -d -p 5000:80 checkoutchallenge-backend
```

To boot up the whole solution integrating with ELK:

```
docker-compose up
```

Might have to set up max virtual memory for the machine, on Linux:

```
sudo sysctl -w vm.max_map_count=262144
```