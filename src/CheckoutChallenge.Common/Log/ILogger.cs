﻿using CheckoutChallenge.Common.Log.Model;
using System.Runtime.CompilerServices;

namespace CheckoutChallenge.Common.Log
{
    public interface ILogger
    {
        void Log<T>(T logObject,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0) where T : BaseLog;
    }
}
