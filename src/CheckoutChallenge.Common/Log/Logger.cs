﻿using CheckoutChallenge.Common.Log.Model;
using CheckoutChallenge.Logger.Interface;
using System;
using System.Runtime.CompilerServices;

namespace CheckoutChallenge.Common.Log
{
    public class Logger : ILogger
    {
        private readonly ILogRepository _logRepository;

        public Logger(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        public void Log<T>(T logObject,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0) where T : BaseLog
        {
            logObject.Date = DateTime.Now;
            logObject.MemberName = memberName;
            logObject.FilePath = filePath;
            logObject.LineNumber = lineNumber;


            var collectionName = "checkoutchallenge.unknown";
            if (logObject is ClientLog)
                collectionName = "checkoutchallenge.client";
            else if (logObject is BasketLog)
                collectionName = "checkoutchallenge.basket";            
            else if (logObject is ProductLog)
                collectionName = "checkoutchallenge.product";
            else if (logObject is ExceptionLog)
                collectionName = "fplogparser.exception";

            _logRepository.InsertAsync(logObject, collectionName);
        }
    }
}
