﻿using System;

namespace CheckoutChallenge.Common.Log.Model
{
    public class ExceptionLog : BaseLog
    {
        public Exception ExceptionObj { get; set; }
    }
}
