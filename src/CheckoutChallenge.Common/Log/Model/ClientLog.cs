﻿namespace CheckoutChallenge.Common.Log.Model
{
    public class ClientLog : BaseLog
    {
        public int ClientId { get; set; }
        public string Operation { get; set; }        
    }
}
