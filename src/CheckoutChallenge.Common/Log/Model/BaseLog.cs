﻿using System;

namespace CheckoutChallenge.Common.Log.Model
{
    public abstract class BaseLog
    {
        public DateTime Date { get; internal set; }
        public string MemberName { get; internal set; }
        public string FilePath { get; internal set; }
        public int LineNumber { get; internal set; }
    }
}
