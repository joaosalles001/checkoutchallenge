﻿namespace CheckoutChallenge.Common.Log.Model
{
    public class BasketLog : BaseLog
    {
        public int ClientId { get; set; }
        public string Operation { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
