﻿namespace CheckoutChallenge.Common.Log.Model
{
    public class ProductLog : BaseLog
    {
        public int ProductId { get; set; }
        public string Operation { get; set; }
    }
}
