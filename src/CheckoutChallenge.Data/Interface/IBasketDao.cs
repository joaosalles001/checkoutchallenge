﻿using CheckoutChallenge.Data.Model;
using System.Collections.Generic;

namespace CheckoutChallenge.Data.Interface
{
    public interface IBasketDao
    {
        Client GetByClient(int clientId);
        bool Contains(int clientId, int productId);
        void AddProduct(int clientId, int productId, int quantity);
        void EditProductQuantity(int clientId, int productId, int newQuantity);
        void RemoveProduct(int clientId, int productId);
        void EmptyBasket(int clientId);
    }
}
