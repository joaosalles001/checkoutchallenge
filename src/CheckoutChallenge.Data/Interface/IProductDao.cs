﻿using CheckoutChallenge.Data.Model;
using System.Collections;
using System.Collections.Generic;

namespace CheckoutChallenge.Data.Interface
{
    public interface IProductDao
    {
        IEnumerable<Product> List();
        Product GetById(int productId);
        Product GetByCode(string productCode);
    }
}
