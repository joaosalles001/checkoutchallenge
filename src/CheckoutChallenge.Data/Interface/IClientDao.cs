﻿using CheckoutChallenge.Data.Model;

namespace CheckoutChallenge.Data.Interface
{
    public interface IClientDao
    {
        Client Create(string name);
        Client GetByName(string name);
    }
}
