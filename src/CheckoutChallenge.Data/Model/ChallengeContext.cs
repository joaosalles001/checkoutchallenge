﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CheckoutChallenge.Data.Model
{
    /// <summary>
    /// The one and only database context
    /// </summary>
    public class ChallengeContext : DbContext
    {
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ClientProduct> ClientProduct { get; set; }
        
        public ChallengeContext(DbContextOptions<ChallengeContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Initialization and declaration of database models
        /// </summary>
        /// <param name="modelBuilder">The model builder</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // initialize many to many relationship
            modelBuilder.Entity<ClientProduct>()
                .HasKey(b => new { b.ClientId, b.ProductId });

            modelBuilder.Entity<ClientProduct>()
                .HasOne(b => b.Client)
                .WithMany(c => c.ClientProducts)
                .HasForeignKey(cp => cp.ClientId);

            modelBuilder.Entity<ClientProduct>()
                .HasOne(cp => cp.Product)
                .WithMany(p => p.ClientProducts)
                .HasForeignKey(cp => cp.ProductId);

            // Seed product data
            modelBuilder.Entity<Product>().HasData(new Product[]
            {
                new Product { Id = 1, Code = "A001", Description = "1st Product", UnitValue = 1.11m },
                new Product { Id = 2, Code = "A002", Description = "2nd Product", UnitValue = 2.22m },
                new Product { Id = 3, Code = "A003", Description = "3rd Product", UnitValue = 3.33m },
                new Product { Id = 4, Code = "A004", Description = "4th Product", UnitValue = 4.44m },
                new Product { Id = 5, Code = "A005", Description = "5th Product", UnitValue = 5.55m },
                new Product { Id = 6, Code = "A006", Description = "6th Product", UnitValue = 6.66m },
                new Product { Id = 7, Code = "A007", Description = "7th Product", UnitValue = 7.77m },
                new Product { Id = 8, Code = "A008", Description = "8th Product", UnitValue = 8.88m }
            });
        }
    }
}
