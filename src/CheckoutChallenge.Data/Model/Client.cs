﻿using System.Collections.Generic;

namespace CheckoutChallenge.Data.Model
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientProduct> ClientProducts { get; set; }
    }
}
