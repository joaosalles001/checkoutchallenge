﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CheckoutChallenge.Data.Model
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public decimal UnitValue { get; set; }

        public virtual ICollection<ClientProduct> ClientProducts { get; set; }
    }
}
