﻿using CheckoutChallenge.Data.Interface;
using CheckoutChallenge.Data.Model;
using System.Linq;

namespace CheckoutChallenge.Data.Implementation
{
    /// <summary>
    /// Data access object for the Client table
    /// </summary>
    public class ClientDao : IClientDao
    {
        private ChallengeContext _dbContext;

        public ClientDao(ChallengeContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Creates a new client
        /// </summary>
        /// <param name="name">The name of the new client</param>
        /// <returns>The filled database object of the new client</returns>
        public Client Create(string name)
        {
            var client = new Client { Name = name };
            _dbContext.Client.Add(client);
            _dbContext.SaveChanges();

            return client;
        }

        /// <summary>
        /// Gets a client by his name
        /// </summary>
        /// <param name="name">The name of the client</param>
        /// <returns>The client if found, null otherwise.</returns>
        public Client GetByName(string name)
        {
            return _dbContext.Client
                .Where(c => c.Name == name)
                .FirstOrDefault();
        }
    }
}
