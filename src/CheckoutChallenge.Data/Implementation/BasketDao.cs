﻿using System.Collections.Generic;
using System.Linq;
using CheckoutChallenge.Data.Interface;
using CheckoutChallenge.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace CheckoutChallenge.Data.Implementation
{
    /// <summary>
    /// Data access object for Basket and BasketProduct tables
    /// </summary>
    public class BasketDao : IBasketDao
    {
        private ChallengeContext _dbContext { get; }

        public BasketDao(ChallengeContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Gets the basket that belongs to a certain customer
        /// </summary>
        /// <param name="clientId">The id of the customer</param>
        /// <returns>The list of products in the basket of the specified customer</returns>
        public Client GetByClient(int clientId)
        {
            return _dbContext.Client
                .Where(c => c.Id == clientId)
                .Include(c => c.ClientProducts)
                .ThenInclude(cp => cp.Product)
                .FirstOrDefault(); 
            
            //return _dbContext.ClientProduct
            //    .Where(cp => cp.ClientId == clientId)
            //    .Include(cp => cp.Product);                
        }

        /// <summary>
        /// Checks if the basket contains a certain product
        /// </summary>
        /// <param name="clientId">The client's id</param>
        /// <param name="productId">The product's id</param>
        /// <returns>True if the basket contains the product, false otherwise</returns>
        public bool Contains(int clientId, int productId)
        {
            return _dbContext.ClientProduct
                .Any(cp => cp.ClientId == clientId && cp.ProductId == productId);
        }

        /// <summary>
        /// Adds an item to the basket of a customer
        /// </summary>
        /// <param name="clientId">The Id of the customer</param>
        /// <param name="productId">The Id of the product</param>
        /// <param name="quantity">The quantity to be added</param>
        public void AddProduct(int clientId, int productId, int quantity)
        {
            _dbContext.ClientProduct.Add(new ClientProduct
            {
                ClientId = clientId,
                ProductId = productId,
                Quantity = quantity
            });

            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Edits the quantity of a specified product in the customer's basket
        /// </summary>
        /// <param name="clientId">The customer's Id</param>
        /// <param name="productId">The product's Id</param>
        /// <param name="newQuantity">The new quantity</param>
        public void EditProductQuantity(int clientId, int productId, int newQuantity)
        {
            var clientProduct = _dbContext.ClientProduct
                .First(cp => cp.ClientId == clientId && cp.ProductId == productId);
            clientProduct.Quantity = newQuantity;

            _dbContext.ClientProduct.Update(clientProduct);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Removes a specified product from the basket
        /// </summary>
        /// <param name="clientId">The customer's Id</param>
        /// <param name="productId">The product's Id</param>
        public void RemoveProduct(int clientId, int productId)
        {
            var clientProduct = _dbContext.ClientProduct
                .First(cp => cp.ClientId == clientId && cp.ProductId == productId);

            _dbContext.Remove(clientProduct);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Empties the basket of a specified customer
        /// </summary>
        /// <param name="clientId">The customer's id</param>
        public void EmptyBasket(int clientId)
        {
            var clientProducts = _dbContext.ClientProduct.Where(cp => cp.ClientId == clientId);
            if (clientProducts.Count() > 0)
            {
                var productList = clientProducts.ToList();
                _dbContext.ClientProduct.RemoveRange(productList);
                _dbContext.SaveChanges();
            }
        }
    }
}
