﻿using System.Collections.Generic;
using System.Linq;
using CheckoutChallenge.Data.Interface;
using CheckoutChallenge.Data.Model;

namespace CheckoutChallenge.Data.Implementation
{
    /// <summary>
    /// Data access object for Product table
    /// </summary>
    public class ProductDao : IProductDao
    {
        private ChallengeContext _dbContext;

        public ProductDao(ChallengeContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Lists every product in the table
        /// </summary>
        /// <returns>The Product list</returns>
        public IEnumerable<Product> List()
        {
            return _dbContext.Product;
        }

        /// <summary>
        /// Gets the product specified by its code
        /// </summary>
        /// <param name="productCode">The code for the product</param>
        /// <returns>The specified product if found, null otherwise.</returns>
        public Product GetByCode(string productCode)
        {
            return _dbContext.Product.FirstOrDefault(p => p.Code == productCode);
        }

        /// <summary>
        /// Gets the product specified by its id
        /// </summary>
        /// <param name="productId">The id for the product</param>
        /// <returns>The specified product if found, null otherwise.</returns>
        public Product GetById(int productId)
        {
            return _dbContext.Product.FirstOrDefault(p => p.Id == productId);
        }        
    }
}
