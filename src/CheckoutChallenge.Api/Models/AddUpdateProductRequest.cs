﻿namespace CheckoutChallenge.Api.Models
{
    public class AddUpdateProductRequest
    {
        public int ClientId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
