namespace CheckoutChallenge.Api.Models
{
    public class BasicIntegerRequest
    {
        public int Value { get; set; }
    }
}
