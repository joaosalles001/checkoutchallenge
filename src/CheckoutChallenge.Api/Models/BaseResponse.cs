﻿namespace CheckoutChallenge.Api.Models
{
    public class BaseResponse<T>
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
