﻿namespace CheckoutChallenge.Api.Models
{
    public class RemoveProductRequest
    {
        public int ClientId { get; set; }
        public int ProductId { get; set; }
    }
}
