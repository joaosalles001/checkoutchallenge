﻿using CheckoutChallenge.Api.Models;
using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Common.Log;
using System;
using CheckoutChallenge.Common.Log.Model;
using Microsoft.AspNetCore.Mvc;

namespace CheckoutChallenge.Api.Controllers
{
    [Route("api/client")]
    public class ClientController : Controller
    {
        private IClientBusiness _clientBusiness;
        private ILogger _logger;

        public ClientController(IClientBusiness clientBusiness, ILogger logger)
        {
            _clientBusiness = clientBusiness;
            _logger = logger;
        }

        /// <summary>
        /// Logs a customer in the system
        /// </summary>
        /// <param name="request">The request object containing the customer's name</param>
        /// <returns>The client object containing name and id</returns>
        [HttpPost]
        [Route("Login")]
        public ActionResult Login([FromBody] BasicTextRequest request)
        {
            var response = new BaseResponse<Bll.Models.Client>();

            try
            {
                var client = _clientBusiness.Login(request.Value);

                response.Data = client;
                _logger.Log(new ClientLog
                {
                    ClientId = client.Id,
                    Operation = "Login"
                });
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Message = e.Message;

                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });
            }

            return Ok(response);
        }
    }
}
