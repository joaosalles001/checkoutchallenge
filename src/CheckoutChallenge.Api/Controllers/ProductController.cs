using System;
using System.Collections.Generic;
using System.Linq;
using CheckoutChallenge.Api.Models;
using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Common.Log;
using CheckoutChallenge.Common.Log.Model;
using Microsoft.AspNetCore.Mvc;

namespace CheckoutChallenge.Api.Controllers
{
    [Route("api/product")]
    public class ProductController : Controller
    {
        private IProductBusiness _productBusiness;
        private ILogger _logger;

        public ProductController(IProductBusiness productBusiness, ILogger logger)
        {
            _productBusiness = productBusiness;
            _logger = logger;
        }

        /// <summary>
        /// Gets the details of a specified product
        /// </summary>
        /// <param name="request">The request object containing the product code</param>
        /// <returns>The product if found, null otherwise</returns>
        [HttpPost]
        [Route("GetByCode")]
        public ActionResult GetByCode([FromBody] BasicTextRequest request)
        {
            var response = new BaseResponse<Bll.Models.Product>();

            try
            {
                var product = _productBusiness.GetByCode(request.Value);

                response.Data = product;
                _logger.Log(new ProductLog
                {
                    ProductId = product.Id,
                    Operation = "GetByCode"
                });
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Message = e.Message;

                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });
            }

            return Ok(response);
        }

        /// <summary>
        /// Lists every product in the database
        /// </summary>
        /// <returns>The list of products</returns>
        [HttpPost]
        [Route("List")]
        public ActionResult List()
        {
            var response = new BaseResponse<List<Bll.Models.Product>>();

            try
            {
                var productList = _productBusiness.List();

                response.Data = productList.ToList();
                _logger.Log(new ProductLog
                {
                    Operation = "List"
                });
            }
            catch (Exception e)
            {
                response.Error = true;
                response.Message = e.Message;

                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });
            }

            return Ok(response);
        }
    }
}