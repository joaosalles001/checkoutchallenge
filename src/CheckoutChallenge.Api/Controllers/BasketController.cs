using System;
using CheckoutChallenge.Api.Models;
using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Common.Log;
using CheckoutChallenge.Common.Log.Model;
using Microsoft.AspNetCore.Mvc;

namespace CheckoutChallenge.Api.Controllers
{
    [Route("api/basket")]
    public class BasketController : Controller
    {
        private IBasketBusiness _basketBusiness;
        private ILogger _logger;

        public BasketController(IBasketBusiness basketBusiness, ILogger logger)
        {
            _basketBusiness = basketBusiness;
            _logger = logger;
        }

        /// <summary>
        /// Gets the product list of a specified client
        /// </summary>
        /// <param name="request">The request object containing the id of the client</param>
        /// <returns>The client's product list</returns>
        [HttpPost]
        [Route("GetByClientId")]
        public ActionResult GetByClientId([FromBody] BasicIntegerRequest request)
        {
            var response = new BaseResponse<Bll.Models.Basket>();

            try
            {
                var basket = _basketBusiness.GetByClientId(request.Value);

                response.Data = basket;
                _logger.Log(new BasketLog
                {
                    Operation = "GetByClientId",
                    ClientId = request.Value
                });
            }
            catch (Exception e)
            {
                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });

                response.Error = true;
                response.Message = e.Message;
            }

            return Ok(response);
        }

        /// <summary>
        /// Adds or updates the quantity of a product in the basket
        /// </summary>
        /// <param name="request">The add/update product request, containing the client's id,
        /// the product's id and the product's quantity</param>
        /// <returns>True if successful, false otherwise.</returns>
        [HttpPost]
        [Route("AddUpdateProduct")]
        public ActionResult AddUpdateProduct([FromBody] AddUpdateProductRequest request)
        {
            var response = new BaseResponse<bool>();

            try
            {
                var success = _basketBusiness.AddUpdateProduct(request.ClientId, request.ProductId, request.Quantity);

                response.Data = success;
                _logger.Log(new BasketLog
                {
                    ClientId = request.ClientId,
                    Operation = "AddUpdateProduct",
                    ProductId = request.ProductId,
                    Quantity = request.Quantity
                });
            }
            catch (Exception e)
            {
                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });

                response.Error = true;
                response.Message = e.Message;
            }

            return Ok(response);
        }

        /// <summary>
        /// Removes a product from the basket
        /// </summary>
        /// <param name="request">The request object containing the client's and the product's ids</param>
        /// <returns>True if successful, false otherwise</returns>
        [HttpPost]
        [Route("RemoveProduct")]
        public ActionResult RemoveProduct([FromBody] RemoveProductRequest request)
        {
            var response = new BaseResponse<bool>();

            try
            {
                var success = _basketBusiness.RemoveProduct(request.ClientId, request.ProductId);

                response.Data = success;
                _logger.Log(new BasketLog
                {
                    ClientId = request.ClientId,
                    Operation = "RemoveProduct",
                    ProductId = request.ProductId
                });
            }
            catch (Exception e)
            {
                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });

                response.Error = true;
                response.Message = e.Message;
            }

            return Ok(response);
        }

        /// <summary>
        /// Removes every product from the basket
        /// </summary>
        /// <param name="request">The request object containing the client's id</param>
        /// <returns>True if successful, false otherwise</returns>
        [HttpPost]
        [Route("EmptyBasket")]
        public ActionResult EmptyBasket([FromBody] BasicIntegerRequest request)
        {
            var response = new BaseResponse<bool>();

            try
            {
                var success = _basketBusiness.ClearBasket(request.Value);

                response.Data = success;
                _logger.Log(new BasketLog
                {
                    ClientId = request.Value,
                    Operation = "EmptyBasket"
                });
            }
            catch (Exception e)
            {
                _logger.Log(new ExceptionLog
                {
                    ExceptionObj = e
                });

                response.Error = true;
                response.Message = e.Message;
            }

            return Ok(response);
        }
    }
}