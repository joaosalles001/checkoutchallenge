﻿using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Bll.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CheckoutChallenge.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using CheckoutChallenge.Data.Implementation;
using CheckoutChallenge.Data.Interface;
using Swashbuckle.AspNetCore.Swagger;
using CheckoutChallenge.Logger.Implementation;
using CheckoutChallenge.Logger.Interface;

namespace CheckoutChallenge.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Log
            var logRepo = new ElasticsearchLogRepository(Configuration.GetConnectionString("Elastic"));
            services.AddSingleton<ILogRepository>(logRepo);
            services.AddScoped<Common.Log.ILogger, Common.Log.Logger>();

            // options builder for the database context. initialize with a new guid so we make sure
            // we're using a different database every time the system boots up - just for testing purposes!
            var optionsBuilder = new DbContextOptionsBuilder<ChallengeContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            // data
            services.AddSingleton(new ChallengeContext(optionsBuilder.Options));
            services.AddScoped<IClientDao, ClientDao>();
            services.AddScoped<IBasketDao, BasketDao>();
            services.AddScoped<IProductDao, ProductDao>();


            // business
            services.AddScoped<IClientBusiness, ClientBusiness>();
            services.AddScoped<IBasketBusiness, BasketBusiness>();
            services.AddScoped<IProductBusiness, ProductBusiness>();

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Checkout Challenge", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
            });

            app.UseMvc();

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Checkout Challenge");
            });
        }
    }
}
