﻿using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Bll.Models;
using CheckoutChallenge.Data.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutChallenge.Bll.Implementation
{
    /// <summary>
    /// Business logic class for Clients
    /// </summary>
    public class ClientBusiness : IClientBusiness
    {
        private IClientDao _clientDao;

        public ClientBusiness(IClientDao clientDao)
        {
            _clientDao = clientDao;
        }

        /// <summary>
        /// Gets the Client from the Database, or Creates a new one if not found
        /// </summary>
        /// <param name="clientName">The client's name</param>
        /// <returns>The newly created or previsouly fetched client</returns>
        public Client Login(string clientName)
        {
            var client = _clientDao.GetByName(clientName);
            if (client == null)
            {
                client = _clientDao.Create(clientName);
            }

            return new Client(client);
        }
    }
}
