using CheckoutChallenge.Bll.Models;
using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Data.Interface;

namespace CheckoutChallenge.Bll.Implementation
{
    public class BasketBusiness : IBasketBusiness
    {
        private IClientDao _clientDao;
        private IBasketDao _basketDao;        

        public BasketBusiness(IClientDao clientDao, IBasketDao basketDao)
        {
            _clientDao = clientDao;
            _basketDao = basketDao;
        }

        /// <summary>
        /// Adds or updates the quantity of a specified product to a client's basket
        /// </summary>
        /// <param name="clientId">The client's id</param>
        /// <param name="productId">The product's id</param>
        /// <param name="quantity">The product's quantity</param>
        /// <returns>True if successful (...)</returns>
        public bool AddUpdateProduct(int clientId, int productId, int quantity)
        {
            if (_basketDao.Contains(clientId, productId))
            {
                _basketDao.EditProductQuantity(clientId, productId, quantity);
            }
            else
            {
                _basketDao.AddProduct(clientId, productId, quantity);
            }

            return true;
        }

        /// <summary>
        /// Clears the client's basket
        /// </summary>
        /// <param name="clientId">The client's id</param>
        /// <returns>True if successful (...)</returns>
        public bool ClearBasket(int clientId)
        {
            _basketDao.EmptyBasket(clientId);

            return true;
        }

        /// <summary>
        /// Gets the basket of a specified client
        /// </summary>
        /// <param name="clientId">The client's id</param>
        /// <returns>The basket object if client is found, null otherwise</returns>
        public Basket GetByClientId(int clientId)
        {
            var clientBasket = _basketDao.GetByClient(clientId);

            return clientBasket == null ? null : new Basket(clientBasket);
        }

        /// <summary>
        /// Gets the basket of a specified client
        /// </summary>
        /// <param name="clientName">The client's name</param>
        /// <returns>The basket object if client is found, null otherwise</returns>
        public Basket GetByClientName(string clientName)
        {
            var client = _clientDao.GetByName(clientName);
            
            return GetByClientId(client.Id);
        }

        /// <summary>
        /// Removes a specified product from the client's basket
        /// </summary>
        /// <param name="clientId">The client's id</param>
        /// <param name="productId">The product's id</param>
        /// <returns>True if successful, false otherwise</returns>
        public bool RemoveProduct(int clientId, int productId)
        {
            bool success;

            if (!_basketDao.Contains(clientId, productId))
            {
                success = false;
            }
            else
            {
                _basketDao.RemoveProduct(clientId, productId);
                success = true;
            }

            return success;            
        }
    }
}