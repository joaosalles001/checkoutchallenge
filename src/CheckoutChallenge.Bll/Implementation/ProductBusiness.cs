using CheckoutChallenge.Bll.Models;
using CheckoutChallenge.Bll.Interfaces;
using CheckoutChallenge.Data.Interface;
using System.Collections.Generic;
using System.Linq;

namespace CheckoutChallenge.Bll.Implementation
{
    public class ProductBusiness : IProductBusiness
    {
        private IProductDao _productDao;

        public ProductBusiness(IProductDao productDao)
        {
            _productDao = productDao;
        }

        /// <summary>
        /// Gets a product's details
        /// </summary>
        /// <param name="productCode">The product's code</param>
        /// <returns>The product if found, null otherwise</returns>
        public Product GetByCode(string productCode)
        {
            var product = _productDao.GetByCode(productCode);
            return product == null ? null : new Product(product);
        }

        /// <summary>
        /// Gets a product's details
        /// </summary>
        /// <param name="productId">The product's id</param>
        /// <returns>The product if found, null otherwise</returns>
        public Product GetById(int productId)
        {
            var product = _productDao.GetById(productId);
            return product == null ? null : new Product(product);
        }

        /// <summary>
        /// Lists every product in the database
        /// </summary>
        /// <returns>The product list</returns>
        public IEnumerable<Product> List()
        {
            return from p in _productDao.List()
                   select new Product(p);
        }
    }
}