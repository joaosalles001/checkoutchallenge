﻿using CheckoutChallenge.Bll.Models;

namespace CheckoutChallenge.Bll.Interfaces
{
    public interface IClientBusiness
    {
        Client Login(string clientName);
    }
}
