
using System.Collections.Generic;
using CheckoutChallenge.Bll.Models;

namespace CheckoutChallenge.Bll.Interfaces
{
    public interface IBasketBusiness
    {
        Basket GetByClientId(int clientId);
        Basket GetByClientName(string clientName);
        bool AddUpdateProduct(int clientId, int productId, int quantity);
        bool RemoveProduct(int clientId, int productId);
        bool ClearBasket(int clientId);
    }
}