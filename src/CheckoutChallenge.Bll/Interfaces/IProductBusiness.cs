
using System.Collections.Generic;
using CheckoutChallenge.Bll.Models;

namespace CheckoutChallenge.Bll.Interfaces
{
    public interface IProductBusiness
    {
        IEnumerable<Product> List();
        Product GetByCode(string productCode);
        Product GetById(int productId);
    }
}