namespace CheckoutChallenge.Bll.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal UnitValue { get; set; }
        public int Quantity { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Product()
        {
        }

        /// <summary>
        /// Translates the database model into a business model
        /// </summary>
        /// <param name="product">The database model for product</param>
        public Product(Data.Model.Product product)
        {
            Id = product.Id;
            Code = product.Code;
            Description = product.Description;
            UnitValue = product.UnitValue;
        }
    }
}