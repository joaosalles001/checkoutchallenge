﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutChallenge.Bll.Models
{
    /// <summary>
    /// Business model for the client
    /// </summary>
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Client()
        {
        }

        public Client(Data.Model.Client client)
        {
            Id = client.Id;
            Name = client.Name;
        }
    }
}
