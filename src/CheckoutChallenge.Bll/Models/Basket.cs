using System.Collections.Generic;
using System.Linq;

namespace CheckoutChallenge.Bll.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public string Client { get; set; }
        public List<Product> ProductList { get; set; } = new List<Product>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public Basket()
        { }

        /// <summary>
        /// Translates database model to business model
        /// </summary>
        /// <param name="basket">The database model</param>
        public Basket(Data.Model.Client clientBasket)
        {
            Id = clientBasket.Id;
            Client = clientBasket.Name;

            if (clientBasket.ClientProducts.Any())
            {
                ProductList = (from cp in clientBasket.ClientProducts
                               select new Product
                               {
                                   Id = cp.Product.Id,
                                   Code = cp.Product.Code,
                                   Description = cp.Product.Description,
                                   UnitValue = cp.Product.UnitValue,
                                   Quantity = cp.Quantity
                               }).ToList();
            }            
        }
    }
}